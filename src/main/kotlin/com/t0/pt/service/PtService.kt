package com.t0.pt.service

import com.t0.pt.dto.PtRequest
import com.t0.pt.dto.PtRequestAttributes
import com.t0.pt.dto.PtRequestData
import com.t0.pt.dto.PtTradeResponse
import org.json.JSONObject
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.stereotype.Service

@EnableFeignClients
@Service
class PtService(val ptClient: PtClient) {

    fun createQuote(
        accId: String,
        asset: String,
        type: String,
        amount: String
    ): String {

        // toggle if u want to mock..
        // webhooks do work with mock trade desk
        val mockTradeDesk = true

        val tradeDeskId = if (mockTradeDesk) {
            "0ecfe116-e2d8-4141-a5bc-0d6ea7670e3e"
        } else {
            ""
        }

        val attributes = PtRequestAttributes(
            accountId = accId,
            assetId = asset,
            totalAmount = amount,
            type = type,
            tradeDeskId = tradeDeskId
        )
        val data = PtRequestData("quotes", attributes)

        val ptRequest = PtRequest(data)
        val quote = ptClient.createQuote(ptRequest)
        println("created the quote $quote for $asset")

        return quote.data.id!!
    }

    fun configureNewAccount(): String {

        // TODO might need new JWT
        val accountResponseJson = ptClient.createAccount(RequestStubs.ACCOUNT_CREATE)

        val jsonObj = JSONObject(
            accountResponseJson.substring(
                accountResponseJson.indexOf("{"),
                accountResponseJson.lastIndexOf("}") + 1
            )
        )
        val accId = (jsonObj.getJSONObject("data") as JSONObject).get("id").toString()
        println("created PT account-id $accId")

        ptClient.openAccount(accId)
        ptClient.fundAccount(accId, RequestStubs.SANDBOX_FUNDS)

        return accId
    }

    fun executeQuote(quoteId: String, asset: String, type: String): PtTradeResponse {
        val tradeResponse = ptClient.executeQuote(quoteId)
        println(
            "A $type trade created for $asset with id ${tradeResponse.data.attributes?.tradeId}"
        )
        return tradeResponse
    }
}

