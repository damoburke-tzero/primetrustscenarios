package com.t0.pt.dto

data class PtTradeResponse(
    val data: PtResponseData,
    val included: List<Any>
)