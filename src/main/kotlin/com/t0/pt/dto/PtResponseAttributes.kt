package com.t0.pt.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class PtResponseAttributes(
    @JsonProperty("base-amount")
    var baseAmount: Double = 0.0,

    @JsonProperty("fee-amount")
    var feeAmount: Double = 0.0,

    @JsonProperty("total-amount")
    var totalAmount: String? = null,

    @JsonProperty("price-per-unit")
    var pricePerUnit: Double = 0.0,

    @JsonProperty("unit-count")
    var unitCount: Double = 0.0,

    @JsonProperty("asset-name")
    var assetName: String? = null,

    var status: String? = null,
    var hot: Boolean = false,

    @JsonProperty("created-at")
    var createdAt: Date? = null,

    @JsonProperty("expires-at")
    var expiresAt: Date? = null,

    @JsonProperty("executed-at")
    var executedAt: Any? = null,

    @JsonProperty("transaction-type")
    var type: String? = null,

    @JsonProperty("trade-id")
    var tradeId: String? = null

)