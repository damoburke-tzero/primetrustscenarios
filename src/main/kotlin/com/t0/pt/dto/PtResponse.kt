package com.t0.pt.dto

data class PtResponse(
    val data: PtResponseData,
    val included: List<Any>
)