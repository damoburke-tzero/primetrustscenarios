package com.t0.pt.dto.accountcashtransfer

data class PtActResponse(
    val data: PtActResponseData,
    val included: List<Any>
)