package com.t0.pt.dto.accountcashtransfer

import com.t0.pt.dto.PtLinks
import com.t0.pt.dto.PtRelationships

data class PtActResponseData(
    val type: String? = null,
    val id: String,
    val links: PtLinks? = null,
    val relationships: PtRelationships? = null,
    val attributes: PtActResponseAttributes? = null
)