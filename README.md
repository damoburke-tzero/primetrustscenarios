# PrimeTrustScenarios

* A SpringBoot app with hacky endpoints to trigger PT APIs on their sandbox env
* Simply set com.t0.pt.dto.Headers.TOKEN. A process for generating JWTs can be found here https://t-zero.atlassian.net/wiki/x/QoAZeg 

Buy and Sell an asset n different times
http://localhost:8080/pt/bs/eth/1
http://localhost:8080/pt/bs/btc/2
http://localhost:8080/pt/bs/ltc/3

Buy all assets
http://localhost:8080/pt/b/all

Buy & Sell all assets
http://localhost:8080/pt/bs/all